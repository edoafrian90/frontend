import React, {Component} from "react";
import {View,Text,TouchableOpacity, Button, TextInput} from 'react-native'
import axios from "axios";

class AdminVoucher extends Component{
  constructor (props){
  super(props);
  this.state = {
    voucher:[],
    name:'',
    price:0
  };
  }
componentDidMount(){
axios.get('https://whichisofficial.com/api/voucher').then(resp => {

    this.setState({voucher:resp.data});
});

}
onDel = (id) => {
    axios.delete(`https://whichisofficial.com/api/voucher/${id}`)
    .then(resp=>{
      alert('voucher dihapus')
      axios.get('https://whichisofficial.com/api/voucher')
      .then(resp => {
      this.setState({voucher:resp.data});
     
  });
    })
  }

  onAdd = () => {
    let tambahVoucher = {
      name : this.state.name,
      price: +this.state.price,
    }
    axios.post(`https://whichisofficial.com/api/voucher`, tambahVoucher)
    .then(resp=>{
      alert(`Anda Menambahkan Voucher ${tambahVoucher.name}, RP ${tambahVoucher.price}`)
      this.setState({name:'',price:0})
      axios.get('https://whichisofficial.com/api/voucher')
      .then(resp => {
      this.setState({voucher:resp.data});
  });
    })
  }

  render(){
    return (
      <View>
        <Text>ADMIN UNTUK VOUCHER</Text> 
       
        <Text>DAFTAR VOUCHER</Text>
        {this.state.voucher.map(item=>{
          return (
            <>
            <Text>ID : {item.id}, Nama : {item.name}, Harga :Rp {item.price.toLocaleString()}  
            </Text>
            <Button title='Hapus' onPress={()=>this.onDel(item.id)}></Button>
            </>
          )
        })}
        <Text>TAMBAH VOUCHER</Text>
        <TextInput placeholder="masukan nama voucher" onChangeText={name=>this.setState({name})} value={this.state.name} ></TextInput>
        <TextInput placeholder="masukan nominal" onChangeText={price=>this.setState({price})} value={this.state.price}></TextInput>
        <Button title='Tambah' onPress={this.onAdd}></Button>

        <TouchableOpacity onPress={() => this.props.navigation.navigate('PESAN PRODUK')}>
          <Text>Pindah ke Pesan Produk </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('ADMIN')}>
          <Text>Ke Halaman Admin Produk </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default AdminVoucher;