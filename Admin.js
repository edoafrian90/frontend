import React, {Component} from "react";
import {View,Text,TouchableOpacity, Button, TextInput} from 'react-native'
import axios from "axios";

class Admin extends Component{
  constructor (props){
  super(props);
  this.state = {
    produk:[],
    nameProduk:'',
    priceProduk:0
  };
  }
componentDidMount(){
  axios.get('https://whichisofficial.com/api/produk')
  .then(resp => {
    this.setState({produk:resp.data});
});
}
onDel = (id) =>{
  axios.delete(`https://whichisofficial.com/api/produk/${id}`)
  .then(resp=>{
    alert('berhasil dihapus')
  })
}
onAdd = () => {
  let tambahProduk = {
    name : this.state.nameProduk,
    price: +this.state.priceProduk
  }
  axios.post('https://whichisofficial.com/api/produk', tambahProduk)
  .then(resp => {
    alert('berhasil ditambahkan')
    this.setState({nameProduk:''})
    this.setState({priceProduk:0})
});
}
  render(){
    return (
      <View>
        <Text>ADMIN UNTUK PRODUK</Text>
        
        <Text>DAFTAR PRODUK</Text>
        {this.state.produk.map(item=>{
          return (
            <>
            <Text>ID : {item.id}, Nama : {item.name}, Harga :Rp {item.price.toLocaleString()}  
            </Text>
            {/* <Button title='edit' onPress={(e)=>onEdit(e)}></Button> */}
            <Button title='Hapus' onPress={()=>this.onDel(item.id)} ></Button>
            </>
          )
        })}
        <Text>TAMBAH PRODUK</Text>
        <TextInput placeholder="masukan nama produk" onChangeText={nameProduk=>this.setState({nameProduk})} ></TextInput>
        <TextInput placeholder="masukan harga" onChangeText={priceProduk=>this.setState({priceProduk})} ></TextInput>
        <Button title='Tambah' onPress={this.onAdd} ></Button>    

        <TouchableOpacity onPress={() => this.props.navigation.navigate('PESAN PRODUK')}>
          <Text>Ke Halaman Pesan Produk </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('ADMIN VOUCHER')}>
          <Text>Ke Halaman Admin Voucher </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Admin;