import React, {Component} from "react";
import {View,Text,TouchableOpacity, TextInput,Button,ScrollView} from 'react-native'
import axios from 'axios'

class PesanProduk extends Component{
  constructor (props){
  super(props);
  this.state = {
    Produk:[],
    name:'',
    price:0,
    jumlahbeli:0
  };
  }
  componentDidMount(){
    axios.get('https://whichisofficial.com/api/produk')
      .then(resp => {
      this.setState({Produk:resp.data});
      });
  }
  onCart = (name,harga) => {
  let tambahcart = {
    name : name,
    price: harga,
    jumlahbeli: +this.state.jumlahbeli
  }
  axios.post(`https://whichisofficial.com/api/cart`, tambahcart)
  .then(resp=>{
    alert(`Anda Pesan ${name}, ${this.state.jumlahbeli} porsi`)
    this.setState({jumlahbeli:''})
  })
}
 
  render(){
    return (
      <ScrollView>
        <Text>DAFTAR MENU</Text>
        {this.state.Produk.map(item=>{
            return(
                <>
                <Text> Nama Produk : {item.name}</Text>
                <Text>Harga : Rp {item.price.toLocaleString()}</Text>
                <TextInput onChangeText={jumlahbeli=>this.setState({jumlahbeli})} placeholder='masukan jumlah pesanan' ></TextInput>
                <Button onPress={()=>this.onCart(item.name,item.price)} title='pesan'  ></Button>
                {/* <TouchableOpacity onPress={()=>this.Uji(item.name)} ><Text>PESAN</Text></TouchableOpacity> */}
                </>
                
            )
        })}
        <TouchableOpacity onPress={() => this.props.navigation.navigate('BAYAR DAN VOUCHER')}>
          <Text>Pindah ke Bayar dan Voucher </Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

export default PesanProduk;