import React, {Component} from "react";
import {View,Text,TouchableOpacity,TextInput, Button} from 'react-native'
import axios from 'axios'

class BayarVoucher extends Component{
  constructor (props){
  super(props);
  this.state = {
    Cart:[],
    OngkosKirim : 0,
    Voucher:[],
    VoucherAktif : 0,
    disableVoucher: false,
    id:0
  };
  }
  componentDidMount(){
    axios.get('https://whichisofficial.com/api/voucher')
    .then(resp => {
    this.setState({Voucher:resp.data});
   
});
    axios.get('https://whichisofficial.com/api/cart')
    .then(resp => {
     this.setState({Cart:resp.data});
  });
  }
  onChange=(e)=>{
     this.setState({OngkosKirim: +e.target.value})
    // console.log(files);
}
 onPress = (price,id) =>{
    this.setState({VoucherAktif: price })
    if(id){
      this.setState({id: id})
    }
    
}
onDel = (id) => {
  axios.delete(`https://whichisofficial.com/api/cart/${id}`)
  .then(resp=>{
    alert('Pesanan dihapus')
    axios.get('https://whichisofficial.com/api/cart')
    .then(resp => {
    this.setState({Cart:resp.data});
   
});
  })
}
  render(){
    var Total = 0
    return (
      <View>
        <Text>DAFTAR PESANAN</Text>
        {this.state.Cart.map(item=>{
            Total += item.price*item.jumlahbeli
            return(
                <>
                <Text> Nama Produk : {item.name}, Harga : Rp {item.price.toLocaleString()}, Jumlah : {item.jumlahbeli}</Text> 
                <TouchableOpacity
                onPress={()=>this.onDel(item.id)}
                style={{ backgroundColor:'red' }}
                ><Text>Hapus Pesanan</Text></TouchableOpacity>              
                </>
                
            )
        })}
        
        <TextInput 
        //  style={styles.input}
         onChangeText={(e)=> this.setState({OngkosKirim: +e})}
         value={this.state.OngkosKirim}
         keyboardType="numeric"
         placeholder="Masukan Ongkos Kirim"
        />
        
        {/* <Button onPress={()=>this.state.OngkosKirim} title='set ongkos kirim'></Button> */}

        <Text>Total Harga Makanan: Rp {Total.toLocaleString()}</Text>
        <Text>Ongkos Kirim: Rp {+this.state.OngkosKirim.toLocaleString()}</Text>
        <Text>Pilih Voucher Aktif </Text>
        {this.state.Voucher.map(item=>{
          return(
            <>
            {/* <Button title='' disabled={Total<=item.nominal ||this.state.disableVoucher? true : false } onPress={this.onPress}></Button> */}
            <TouchableOpacity 
            disabled={Total < 10*item.price ||item.id === this.state.id? true : false } 
            onPress={()=>this.onPress(item.price, item.id)}
            style={Total < 10*item.price || item.id === this.state.id? {backgroundColor:'pink'}:{backgroundColor:'aqua'}}
            key={item.id}
            >
              <Text>{item.name} Status:{Total<10*item.price ||item.id === this.state.id? 'Tidak AKtif':'Aktif'}</Text>
            </TouchableOpacity>
            </>
          )
        })}
        {/* <Button title='Voucher 5K' disabled={Total<=50000||this.state.disableVoucher? true : false} onPress={this.onPress} ></Button> */}
        <Text>Potongan dari Voucher : Rp {this.state.VoucherAktif} </Text>
        <Text>Harga yang dibayar : Rp {(Total+this.state.OngkosKirim - this.state.VoucherAktif).toLocaleString()}</Text>
        
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('ADMIN')}>
          <Text>Kembali Ke halaman Admin</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('PESAN PRODUK')}>
          <Text>Ke Halaman Pesan </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default BayarVoucher;