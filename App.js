/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import * as React from 'react';
 import { NavigationContainer } from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack'

const Stack = createNativeStackNavigator();
import Admin from './Admin';
import PesanProduk from './PesanProduk';
import BayarVoucher from './BayarDanVoucher';
import AdminVoucher from './AdminVoucher';
 
 const App = () => {
   return (
     <NavigationContainer>
     <Stack.Navigator>
      <Stack.Screen name='ADMIN' component={Admin}/>
      <Stack.Screen name='ADMIN VOUCHER' component={AdminVoucher}/>
      <Stack.Screen name='PESAN PRODUK' component={PesanProduk}/>
      <Stack.Screen name='BAYAR DAN VOUCHER' component={BayarVoucher}/>
     </Stack.Navigator>
     </NavigationContainer>
   );
 };
 
 export default App;